(require :asdf)

(defpackage bitacora-system
  (:use :cl :asdf))


(in-package :bitacora-system)

(defsystem bitacora
    :description "Bitacora is Documentation Generator"
    :version "0.1"
    :author "Mariano Montone <marianomontone@gmail.com>"
    :maintainer "Mariano Montone <marianomontone@gmail.com>"
    :licence "Public Domain"
    :components
    ((:module src
	      :components
	      ((:file "package"))))
    :depends-on
    (:montezuma))